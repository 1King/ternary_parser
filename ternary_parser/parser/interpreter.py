"""
This is contains classes to obtain the semantics of the AST.

The algorithm used is recursive descent parsing

"""

import copy
import operator
from collections import deque
from dataclasses import dataclass, field
from typing import Any, Callable, Deque, Optional, Union

from ternary_parser.grammar.grammar import BINARY_OPERATORS, KEYWORDS
from ternary_parser.parser.parser import Token

OPERATORS = {
    "+": operator.add,
    "-": operator.sub,
    "*": operator.mul,
    "/": operator.truediv,
    "==": operator.eq,
    "!=": operator.ne,
    "<=": operator.le,
    ">=": operator.ge,
    "<": operator.lt,
    ">": operator.gt,
    "|": operator.or_,
    "&": operator.and_,
    "~": operator.not_,
}


class SemanticsError(Exception):
    pass


@dataclass
class Node:
    name: str
    value: Any
    operator: Any = field(default="null")
    is_final: bool = field(default=True)


class ASTVisitor:
    def __init__(self, ast: Deque[Union[str, Token]]):
        self.AST = copy.deepcopy(ast)
        self.ast_index = 0

        self.var_index = 1

        self.program_vars = {"T": True, "F": False, "True": True, "False": False}

        self.stmt_outputs: Deque = deque()

        # Todo: in future
        self.intermediate_code: Deque = deque()

        self.function_mappings = {
            "PROGRAM": self.visit_program,
            "<stmt-list>": self.visit_stmt_list,
            "STMT": self.visit_stmt,
            "<declarations>": self.visit_declarations,
            "<var-dec>": self.visit_var_dec,
            "<more-var-dec>": self.visit_more_var_dec,
            "EXP": self.visit_exp,
            "EXP2": self.visit_exp2,
            "<disjunction>": self.visit_disjunction,
            "<more-disjunction>": self.visit_more_disjunction,
            "<conjunction>": self.visit_conjunction,
            "<more-conjunction>": self.visit_more_conjunction,
            "TERM": self.visit_term,
            "TERM2": self.visit_term2,
            "FACTOR": self.visit_factor,
            "FACTOR2": self.visit_factor2,
            "<ternary>": self.visit_ternary,
            "<condition>": self.visit_exp,
            "<truthy>": self.visit_exp,
            "<falsey>": self.visit_exp,
            "NAME": self.visit_name,
            "NUMBER": self.visit_number,
            "DEFAULT": self.move_to_next,
        }

    def traverse(self, argument_node: Optional[Node] = None) -> Node:
        node = Node(f"temp_var_{self.var_index}", 0)
        tree_node = self.get_tree_node(self.ast_index)

        if tree_node == "AST_END":
            # no more traversing, AST end is reached
            return node

        evaluate_program = self.find_function(tree_node)  # type: ignore

        if argument_node is not None:
            node = evaluate_program(argument_node)
        else:
            node = evaluate_program()

        return node

    def get_tree_node(self, index: int) -> Union[str, Token]:
        if self.ast_index >= len(self.AST):
            return "AST_END"
        return self.AST[index]

    def move_to_next(self, step: int = 1, position: str = None) -> Node:
        node = Node(f"temp_var_{self.var_index}", 0)
        self.increment_var_index()
        if position is not None:
            while self.ast_index < len(self.AST):
                tree_node = self.AST[self.ast_index]
                if tree_node == position:
                    return node
                self.ast_index += 1
        else:
            self.ast_index += step
        return node

    def increment_var_index(self) -> None:
        self.var_index += 1
        return

    def find_function(self, tree_node: Union[str, Token]) -> Callable[..., Any]:
        """Find the function we are going to use to derive meaning."""
        if isinstance(tree_node, str):
            return self.function_mappings.get(tree_node) or self.function_mappings.get(
                "DEFAULT"
            )  # type: ignore
        else:
            # Token types in the AST
            return self.move_to_next

    def visit_program(self) -> Node:
        node = Node(f"temp_var_{self.var_index}", 0)
        self.increment_var_index()
        self.move_to_next()
        returned_node = self.traverse()
        node.value = returned_node.value
        self.stmt_outputs.append(node)
        self.move_to_next(position="<stmt-list>")
        self.traverse()
        return node

    def visit_stmt_list(self) -> Node:
        node = Node(f"temp_var_{self.var_index}", 0)
        self.increment_var_index()
        self.move_to_next()
        returned_node = self.traverse()
        node.value = returned_node.value
        self.stmt_outputs.append(node)
        self.move_to_next(position="<stmt-list>")
        self.traverse()
        return node

    def visit_stmt(self) -> Node:
        node = Node(f"temp_var_{self.var_index}", 0)
        self.increment_var_index()
        returned_node = self.move_to_next()
        tree_node = self.get_tree_node(self.ast_index)
        if tree_node == ";":
            return returned_node
        else:
            saved_ast_index = self.ast_index
            # first get the variable declarations then execute the statements
            self.move_to_next(position="<declarations>")
            self.traverse()

            # next execute the statements
            self.ast_index = saved_ast_index
            self.move_to_next(position="EXP")
            returned_node = self.traverse()

            node.value = returned_node.value
            return node

    def visit_declarations(self) -> Node:
        node = Node(f"temp_var_{self.var_index}", 0)
        self.increment_var_index()
        self.move_to_next()
        tree_node = self.get_tree_node(self.ast_index)
        self.traverse()
        if tree_node == ",":
            self.move_to_next(position="<var-dec>")
            self.traverse()
        return node

    def visit_var_dec(self) -> Node:
        node = Node(f"temp_var_{self.var_index}", 0)
        self.increment_var_index()
        self.move_to_next(2)
        tree_node = self.get_tree_node(self.ast_index)
        variable_name = tree_node.token_value  # type: ignore

        if variable_name in KEYWORDS:
            raise SemanticsError(
                f"Do not declare variables using {variable_name}. That is a keyword."
            )

        self.move_to_next(position="EXP")
        value_node = self.traverse()
        self.program_vars[variable_name] = value_node.value
        self.intermediate_code.extend(
            [f"{variable_name}=[]", f"{variable_name}[0]={value_node.value}"]
        )
        self.move_to_next(position="<more-var-dec>")
        self.traverse()
        return node

    def visit_more_var_dec(self) -> Node:
        node = Node(f"temp_var_{self.var_index}", 0)
        self.increment_var_index()
        self.move_to_next()
        tree_node = self.get_tree_node(self.ast_index)
        if tree_node == "$":
            return node
        else:
            self.move_to_next(position="<var-dec>")
            node.value = self.traverse().value
        return node

    def visit_exp(self) -> Node:
        node = Node(f"temp_var_{self.var_index}", 0)
        self.increment_var_index()

        self.move_to_next(position="<disjunction>")
        left_side_node = self.traverse()

        self.move_to_next(position="EXP2")
        node.value = self.traverse(left_side_node).value

        return node

    def visit_ternary(self) -> Node:
        node = Node(f"temp_var_{self.var_index}", 0)
        self.increment_var_index()
        self.move_to_next(position="<condition>")
        condition_node = self.traverse()
        self.move_to_next(position="<truthy>")
        truthy_node = self.traverse()
        self.move_to_next(position="<falsey>")
        falsey_node = self.traverse()

        node.value = (
            float(truthy_node.value)
            if condition_node.value
            else float(falsey_node.value)
        )

        return node

    def visit_exp2(self, previous_left_side_node: Node) -> Node:
        node = Node(f"temp_var_{self.var_index}", 0)
        self.increment_var_index()

        self.move_to_next()
        operator = self.get_tree_node(self.ast_index)

        # Todo: I can be more specific here
        if operator not in BINARY_OPERATORS:
            # this is the last EXP2
            return previous_left_side_node

        self.move_to_next(position="<disjunction>")
        left_side_node = self.traverse()

        node.value = OPERATORS[operator](  # type: ignore
            int(previous_left_side_node.value), int(left_side_node.value)  # type: ignore
        )

        self.move_to_next(position="EXP2")
        node.value = self.traverse(node).value

        return node

    def visit_disjunction(self) -> Node:
        node = Node(f"temp_var_{self.var_index}", 0)
        self.increment_var_index()

        self.move_to_next(position="<conjunction>")
        left_side_node = self.traverse()

        self.move_to_next(position="<more-disjunction>")
        node.value = self.traverse(left_side_node).value

        return node

    def visit_more_disjunction(self, previous_left_side_node: Node) -> Node:
        node = Node(f"temp_var_{self.var_index}", 0)
        self.increment_var_index()

        self.move_to_next()
        operator = self.get_tree_node(self.ast_index)

        # Todo: I can be more specific here
        if operator not in BINARY_OPERATORS:
            # This is the last <more-disjunction>
            return previous_left_side_node

        self.move_to_next(position="<conjunction>")
        left_side_node = self.traverse()

        node.value = OPERATORS[operator](  # type: ignore
            int(previous_left_side_node.value), int(left_side_node.value)
        )

        self.move_to_next(position="<more-disjunction>")
        self.traverse(node).value

        return node

    def visit_conjunction(self) -> Node:
        node = Node(f"temp_var_{self.var_index}", 0)
        self.increment_var_index()

        self.move_to_next(position="TERM")
        left_side_node = self.traverse()

        self.move_to_next(position="<more-conjunction>")
        node.value = self.traverse(left_side_node).value

        return node

    def visit_more_conjunction(self, previous_left_side_node: Node) -> Node:
        node = Node(f"temp_var_{self.var_index}", 0)
        self.increment_var_index()

        self.move_to_next()
        operator = self.get_tree_node(self.ast_index)

        # Todo: I can be more specific here
        if operator not in BINARY_OPERATORS:
            # this is the last <more-conjunction>
            return previous_left_side_node

        self.move_to_next(position="TERM")
        left_side_node = self.traverse()

        node.value = OPERATORS[operator](  # type: ignore
            float(previous_left_side_node.value), float(left_side_node.value)  # type: ignore
        )

        self.move_to_next(position="<more-conjunction>")
        node.value = self.traverse(node).value

        return node

    def visit_term(self) -> Node:
        node = Node(f"temp_var_{self.var_index}", 0)
        self.increment_var_index()

        self.move_to_next(position="FACTOR")
        left_side_node = self.traverse()

        self.move_to_next(position="TERM2")
        node.value = self.traverse(left_side_node).value

        return node

    def visit_term2(self, previous_left_side_node: Node) -> Node:
        node = Node(f"temp_var_{self.var_index}", 0)
        self.increment_var_index()

        self.move_to_next()
        operator = self.get_tree_node(self.ast_index)

        # Todo: I can be more specific here
        if operator not in BINARY_OPERATORS:
            # this is the last TERM2
            return previous_left_side_node

        self.move_to_next(position="FACTOR")
        left_side_node = self.traverse()

        node.value = OPERATORS[operator](  # type: ignore
            float(previous_left_side_node.value), float(left_side_node.value)
        )

        self.move_to_next(position="TERM2")
        node.value = self.traverse(node).value

        return node

    def visit_factor(self) -> Node:
        node = Node(f"temp_var_{self.var_index}", 0)
        self.increment_var_index()

        self.move_to_next()
        operator = self.get_tree_node(self.ast_index)

        # Todo: I can be more specific here
        if operator not in BINARY_OPERATORS:
            factor = self.traverse()
            node.value = factor.value
            return node

        self.move_to_next(position="FACTOR")
        factor = self.traverse()
        node.value = OPERATORS[operator](float(0), float(factor.value))  # type: ignore

        return node

    def visit_factor2(self) -> Node:
        node = Node(f"temp_var_{self.var_index}", 0)
        self.increment_var_index()

        self.move_to_next()
        tree_node = self.get_tree_node(self.ast_index)

        if tree_node == "(":
            self.move_to_next(position="EXP")
            node.value = self.traverse().value
            return node

        if tree_node == "~":
            self.move_to_next(position="FACTOR2")
            factor_value = self.traverse().value
            node.value = OPERATORS[tree_node](float(factor_value))  # type: ignore
            return node

        node.value = self.traverse().value
        return node

    def visit_name(self) -> Node:
        node = Node(f"temp_var_{self.var_index}", 0)
        self.increment_var_index()

        self.move_to_next()
        token = self.get_tree_node(self.ast_index)
        node.name = token.token_value  # type: ignore
        node.value = self.program_vars.get(token.token_value)  # type: ignore
        if node.value is None:
            raise SemanticsError(
                f"Trying to access variable {node.name} before it has been declared."
            )
        self.intermediate_code.append(f"{token.token_value}={token.token_value}[0]")  # type: ignore
        return node

    def visit_number(self) -> Node:
        node = Node(f"temp_var_{self.var_index}", 0)
        self.increment_var_index()
        self.move_to_next()
        token = self.get_tree_node(self.ast_index)
        node.value = token.token_value  # type: ignore
        self.intermediate_code.append(
            f"temp_var{self.var_index}={token.token_value}"  # type: ignore
        )
        return node
