"""
General parsing idea:.

have the tokens in a stack
have your grammar defined somewhere using the dict like structure
have your stack of abstract syntax tree to navigate the tokens
AST
Parse Tree
"""
import copy
from collections import deque
from dataclasses import dataclass
from tokenize import TokenInfo
from typing import Any, Deque

from ternary_parser.grammar.grammar import GRAMMAR, Lexeme


@dataclass
class Token:
    __slots__ = ["token_info", "token_name", "token_value"]
    token_info: TokenInfo
    token_name: str
    token_value: Any


class ParsingError(Exception):
    def __init__(self, lexeme: str, token: Token):
        self.lexeme = lexeme
        self.token = token
        print(
            f"ParsingError: Was expecting to match grammar item: {self.lexeme} but was given "
            f"token: ({self.token.token_value}), starting at row: {self.token.token_info.start[0]} "
            f"column: {self.token.token_info.start[1]} in line '{self.token.token_info.line}'"
        )


class Parser:
    def __init__(self, symbol_table: list, lexemes: Lexeme):
        # create a stack to hold the lexemes
        self.parse_tree: Deque = deque()
        self.AST: Deque = deque()
        self.lexemes = copy.deepcopy(lexemes)
        self.symbol_table = copy.deepcopy(symbol_table)
        self.current_symbol = -1

    def parse(self) -> None:
        self.parse_tree.appendleft(self.lexemes.start)

        while True:
            if len(self.parse_tree) > 0:
                current_lexeme = self.get_top_lexeme()
            else:
                # stack is empty
                return

            if (self.current_symbol + 1) < len(self.symbol_table):
                lookup_symbol = self.get_lookup_token()
            else:
                # end of input, prepare to exit
                print(self.parse_tree[0])
                self.AST.append(self.parse_tree.popleft())
                continue

            if lookup_symbol.token_value == "":
                # convert the null symbol to something useful
                lookup_symbol.token_value = self.lexemes.end

            if current_lexeme in self.lexemes.non_terminal:
                expanded_lexeme = (
                    GRAMMAR[current_lexeme].get(lookup_symbol.token_value)
                    or GRAMMAR[current_lexeme].get(lookup_symbol.token_name)
                    or GRAMMAR[current_lexeme].get(self.lexemes.default)
                )

                if not expanded_lexeme:
                    raise ParsingError(current_lexeme, lookup_symbol)
                print(self.parse_tree[0])
                self.AST.append(self.parse_tree.popleft())
                self.parse_tree.extendleft(reversed(expanded_lexeme))
                continue

            if current_lexeme in self.lexemes.terminal:
                print(self.parse_tree[0])
                self.AST.append(self.parse_tree.popleft())
                if (
                    current_lexeme == lookup_symbol.token_value
                    or current_lexeme == lookup_symbol.token_name
                ):
                    print(lookup_symbol)
                    self.AST.append(lookup_symbol)
                    self.consume_symbol()
                else:
                    if current_lexeme != self.lexemes.end:
                        raise ParsingError(current_lexeme, lookup_symbol)
                continue

    def get_lookup_token(self) -> Token:
        return self.symbol_table[self.current_symbol + 1]

    def get_top_lexeme(self) -> str:
        return self.parse_tree[0]

    def consume_symbol(self) -> None:
        self.current_symbol += 1
        return
