"""
The LL(1) grammar.

GRAMMAR {
    NON_TERMINAL : {
        "LOOKUP_TOKEN": [ REPLACEMENT GRAMMAR RULE ]
    }
}

The DEFAULT key is used to match every_other token that is not null
.
"""
from dataclasses import dataclass
from typing import Set

START = "PROGRAM"

DEFAULT = "DEFAULT"

END = "$"

NON_TERMINAL = {
    START,
    "<stmt-list>",
    "STMT",
    "<declarations>",
    "<var-dec>",
    "<more-var-dec>",
    "EXP",
    "EXP2",
    "<disjunction>",
    "<more-disjunction>",
    "<conjunction>",
    "<more-conjunction>",
    "TERM",
    "TERM2",
    "FACTOR",
    "FACTOR2",
    "<ternary>",
    "<condition>",
    "<truthy>",
    "<falsey>",
}

BINARY_OPERATORS = {
    "==",
    "!=",
    ">=",
    "<",
    "<=",
    "<",
    "+",
    "-",
    "*",
    "/",
    "|",
    "&",
}

UNARY_OPERATORS = {"~", "-", "+"}

KEYWORDS = {"run", "if", "True", "False", "T", "F"}

TERMINAL = {
    END,
    ";",
    ",",
    ":",
    "if",
    "run",
    "+",
    "-",
    "*",
    "/",
    "(",
    ")",
    "{",
    "}",
    "`",
    "~",
    "|",
    "&",
    "==",
    "!=",
    ">=",
    ">",
    "<=",
    "<",
    "NAME",
    "NUMBER",
}

GRAMMAR = {
    "PROGRAM": {
        DEFAULT: ["<stmt-list>", "PROGRAM"],
        END: [END],
    },
    "<stmt-list>": {
        DEFAULT: ["STMT", "<stmt-list>"],
        END: [END],
    },
    "STMT": {
        "run": ["run", "(", "`", "EXP", "`", "<declarations>", ")", ";"],
        ";": [";"],
    },
    "<declarations>": {
        ",": [
            ",",
            "{",
            "<var-dec>",
            "}",
        ],
        DEFAULT: [END],
    },
    "<var-dec>": {
        "NAME": ["NAME", ":", "EXP", "<more-var-dec>"],
    },
    "<more-var-dec>": {
        ",": [",", "<var-dec>"],
        DEFAULT: [END],
    },
    "EXP": {
        DEFAULT: ["<disjunction>", "EXP2"],
    },
    "EXP2": {
        "|": ["|", "<disjunction>", "EXP2"],
        DEFAULT: [END],
    },
    "<disjunction>": {
        DEFAULT: ["<conjunction>", "<more-disjunction>"],
    },
    "<more-disjunction>": {
        "&": ["&", "<conjunction>", "<more-disjunction>"],
        DEFAULT: [END],
    },
    "<conjunction>": {
        DEFAULT: ["TERM", "<more-conjunction>"],
    },
    "<more-conjunction>": {
        "+": ["+", "TERM", "<more-conjunction>"],
        "-": ["-", "TERM", "<more-conjunction>"],
        "==": ["==", "TERM", "<more-conjunction>"],
        "!=": ["!=", "TERM", "<more-conjunction>"],
        "<": ["<", "TERM", "<more-conjunction>"],
        "<=": ["<=", "TERM", "<more-conjunction>"],
        ">": [">", "TERM", "<more-conjunction>"],
        ">=": [">=", "TERM", "<more-conjunction>"],
        DEFAULT: [END],
    },
    "TERM": {DEFAULT: ["FACTOR", "TERM2"]},
    "TERM2": {
        "*": ["*", "FACTOR", "TERM2"],
        "/": ["/", "FACTOR", "TERM2"],
        DEFAULT: [END],
    },
    "FACTOR": {"+": ["+", "FACTOR"], "-": ["-", "FACTOR"], DEFAULT: ["FACTOR2"]},
    "FACTOR2": {
        "NAME": ["NAME"],
        "NUMBER": ["NUMBER"],
        "if": ["<ternary>"],
        "(": ["(", "EXP", ")"],
        "~": ["~", "FACTOR2"],
    },
    "<ternary>": {
        "if": ["if", "(", "<condition>", ",", "<truthy>", ",", "<falsey>", ")"],
    },
    # basically just a copy of the EXP
    "<condition>": {
        DEFAULT: ["EXP"],
    },
    "<truthy>": {
        DEFAULT: ["EXP"],
    },
    "<falsey>": {
        DEFAULT: ["EXP"],
    },
}


@dataclass(kw_only=True)  # type: ignore
class Lexeme:
    __slots__ = ["non_terminal", "terminal", "start", "end", "default"]

    non_terminal: Set[str]
    terminal: Set[str]
    start: str
    end: str
    default: str
