"""Lexer."""
import re
import tokenize
from io import StringIO


class Scanner:
    """The lexer class of MiniTernary."""

    def __init__(self, file_path: str):
        """Identify the tokens from the source file.

        This uses python's tokenize generator
        library module.

        Args:
            file_path: absolute file path

        Returns: None
        """
        token_list = []

        with tokenize.open(file_path) as f:
            file_string = f.read()
            # remove comments from the file string
            # comments are C - style comments i.e /* ... */ and //....\n
            file_string = re.sub(
                r"//.*?(?=\n|$)|/\*[\s\S]*?\*/", "", file_string, flags=re.DOTALL
            )
            tokens = tokenize.generate_tokens(StringIO(file_string).readline)
            token_list = list(tokens)

        self.token_list = self.remove_unwanted_tokens(token_list)

    def remove_unwanted_tokens(self, token_list: list) -> list:
        """
        Remove unwanted tokens as specified in tokens to remove.

        Args:
            token_list: list of tokens

        Returns:
            list: sanitized list.
        """
        tokens_to_remove = [
            "\n",
            "\t",
            " ",
        ]
        remove_indices = []

        for i, token in enumerate(token_list):
            if token.string in tokens_to_remove:
                remove_indices.append(i)

        token_list[:] = [
            token for i, token in enumerate(token_list) if i not in remove_indices
        ]

        return token_list
