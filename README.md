# The project

This is a small interpretive parser for a mini language closely based on python and whose grammar is as defined in the Documentation section of this document

## Tech Stack

**Building:** Python, Poetry, Pip

**Documentation:** Markdown, plain text

## Setting up the environment
(I assume you already have poetry installed. Otherwise, first install poetry using pip or follow this [poetry install link](https://python-poetry.org/docs/) for more instructions)

Once you are sure you have poetry:

cd into the root directory. Then execute the following bash commands:

`>>> poetry shell`

`>>> poetry install`

`>>> pre-commit install`


## RUN

from the root folder:
```
python run.py    # this will run the sample input file by default

python run.py --file <absolute_file_path>  # this is used to parse an arbitrary file
```
## Screenshots
test_2.mt being parsed
![Output of test2 being run](/ternary_parser/docs/screenshot1.png)

multiple stmts running in test_3.mt
![Output of multiple stmts in test_3](ternary_parser/docs/screenshot2.png)
## Tests
The tests are contained in the tests folder. Feel free to run any of them as they are very intuitive and helpful in terms of understanding more of the grammar.

You can also modify the file that is run when you execute the run.py file to any of the test file in existence

## Features
### A well structured grammar
    The structure of the grammar is simplified to LL(1) to allow parsing using a recursive descent parser (i.e. only one lookup token). This makes it simple to add new rules and features to the grammar in future.

### C-Style muli-line and single line comments

```C
/*  ....... */

and

// ...... [ \n ]
```
### Multiple statement executions
    run(`(T | F) & (~T)`);
    run(`45`);
### Ternary operations:
    run(`if (<expression>, <value_if_true>, <value_if_false>`));
### Boolean Operations:
~ for not

| for or

& for and
```C
    run(`~T | F`);
    run(`((T & ~(F | T)) == F) != ((T > F) | (F >= T))`);   /* This is False */
```
### Variable Declarations:
```C
        run(`if(a==8, 2, if(b <= 7, 9, 4))`, {a:6, b:4}); /* This outputs 9. The file for this is test_2.mt*/
```
## Checklist
- [x] Defining the grammar of mini_ternary

- [x] Lexing - Identifying tokens correctly, cleaning unusable tokens (comments etc)

- [x] Syntax Analysis - Identifying grammar errors.
- [x] Semantic Analysis - Traversing the AST through recursive descent parsing and generating the different meanings.
- [x] Correct Arithmetic Operator Associativity - The minilanguage is now using the standard order of operations and is evaluating correctly arithmetic operations from left to right after the grammar rules were updated. It is now not a must to be precise with the order of expressions using brackets since the language is now in standard form.
## Todo
- [ ] code base improvement, some earmarked areas with Todos and also look at ways of reducing the codebase.
### Nice to haves for the future
- [ ] it might be cool to complete the generation of intermediate code so that if one wishes to improve the compilation later to assembly, you might be able to see areas to improve the mini language from a generated Intermediate code file.

## Authors

- [@Cephas-Kingori [github]](https://www.github.com/Cephas-Kingori)
- [@Cephas_Kingori [twitter]](https://www.twitter.com/Cephas_Kingori)

## Acknowledgements

 - [README generator](https://readme.so/editor)
 - [.gitignore generator](https://www.toptal.com/developers/gitignore/)
 - [Awesome Readme Templates](https://awesomeopensource.com/project/elangosundar/awesome-README-templates)
 - [Awesome README](https://github.com/matiassingers/awesome-readme)
 - [How to write a Good readme](https://bulldogjob.com/news/449-how-to-write-a-good-readme-for-your-github-project)
 - [Licence Agreement (T & C)](https://choosealicense.com/licenses/agpl-3.0/#)

  ### REFERENCES
 - [python grammar](https://docs.python.org/3/reference/grammar.html)
-  [Everything to know about compilers and parsers](https://craftinginterpreters.com/scanning.html)
## Docs
[grammar definition](https://gitlab.com/1King/ternary_parser/-/blob/main/ternary_parser/docs/ckingori_Modified_Ternary_Perser_Grammar.docx)


## Badges

[![AGPL License](https://img.shields.io/badge/license-AGPL-blue.svg)](http://www.gnu.org/licenses/agpl-3.0)
